# data "terraform_remote_state" "vpc" {
#   backend = "local"
#   config = {
#     path = "../vpc/terraform.tfstate"
#    }
# }

data "aws_key_pair" "generated_key" {
  key_name = var.key_name
  depends_on = [
    aws_key_pair.generated_key
  ]
}

resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  count    = var.create_new_key_pair ? 1 : 0
  key_name   = var.key_name
  public_key = tls_private_key.example.public_key_openssh
}

resource "local_file" "ssh_key" {
  filename             = "${var.key_name}.pem"
  file_permission      = "400"
  directory_permission = "700"
  content              = tls_private_key.example.private_key_pem
}



module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = var.name

  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  monitoring                  = var.monitoring
  vpc_security_group_ids      = [var.vpc_security_group_ids]
  subnet_id                   = var.subnet_id
  availability_zone           = var.availability_zone
  associate_public_ip_address = var.associate_public_ip_address
  user_data                   = "${file("./software.sh")}"
  depends_on = [
    aws_key_pair.generated_key
  ]
  ebs_block_device = [{
    device_name           = var.device_name
    volume_type           = var.volume_type
    volume_size           = var.volume_size
    delete_on_termination = var.delete_on_termination
    encrypted             = var.encrypted
    iops                  = var.iops
    kms_key_id            = var.kms_key_id
    snapshot_id           = var.snapshot_id
  }]
  tags = {
    ApplicationName  = var.ApplicationName
    ApplicationOwner = var.ApplicationOwner
    Environment      = var.Environment
    EmailID          = var.EmailID
    Business         = var.Business
  }

  # network_interface = [ 
  #   {
  #     network_interface_id = aws_network_interface.example.id
  #     device_index = 0
  #   }
  #  ]
}

# resource "aws_network_interface" "example" {
#   subnet_id       = var.subnet_id
#   # ipv6_addresses = [ "10.0.1.0" ]

#   security_groups = [var.vpc_security_group_ids]
  
# }
