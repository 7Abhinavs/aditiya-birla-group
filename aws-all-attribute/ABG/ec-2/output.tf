output "key_name" {
  value = data.aws_key_pair.generated_key.key_name
}

output "ec2_instance" {
  value = module.ec2_instance.public_ip
}

output "ec2_instance-id" {
  value = module.ec2_instance.id
}

output "tags" {
  value = module.ec2_instance.tags_all
}

# output "aws_network_interface-id" {
#   value = aws_network_interface.example.id
# }
