variable "name" {

}

variable "ami" {

}

variable "instance_type" {

}

variable "key_name" {

}

variable "monitoring" {

}

variable "vpc_security_group_ids" {

}

variable "subnet_id" {

}

variable "cpu_core_count" {

}

variable "cpu_threads_per_core" {

}

variable "cpu_credits" {

}

variable "disable_api_termination" {

}

variable "device_name" {

}

variable "volume_type" {

}

variable "volume_size" {

}

variable "delete_on_termination" {

}

variable "encrypted" {

}

variable "iops" {

}

variable "kms_key_id" {

}

variable "snapshot_id" {

}


# variable "ebs_block_device" {
#   device_name           = ""
#   volume_type           = ""
#   volume_size           = ""
#   delete_on_termination = ""
#   encrypted             = ""
#   iops                  = ""
#   kms_key_id            = ""
#   snapshot_id           = ""
#   throughput            = ""
# }

variable "source_dest_check" {

}

variable "create_spot_instance" {

}

variable "iam_instance_profile" {

}

variable "ebs_optimized" {

}

variable "enable_volume_tags" {

}

variable "hibernation" {

}

variable "ipv6_address_count" {

}

variable "ipv6_addresses" {

}

variable "launch_template" {

}

variable "private_ip" {

}

variable "tenancy" {

}

variable "Environment" {

}

variable "role_name" {

}

variable "availability_zone" {

}

variable "associate_public_ip_address" {

}

variable "ApplicationName" {

}

variable "ApplicationOwner" {

}

variable "EmailID" {

}

variable "Business" {

}

variable "target_group_arn" {

}

variable "target_id" {

}

variable "port" {

}

variable "create_new_key_pair" {
  
}
