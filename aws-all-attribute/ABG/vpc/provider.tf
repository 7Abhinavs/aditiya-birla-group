provider "aws" {
  version = "4.53.0"
  assume_role {
    role_arn = "arn:aws:iam::981359768303:role/terraform"
  }
  region = "ap-south-1"
}
