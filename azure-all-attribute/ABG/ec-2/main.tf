

data "azurerm_ssh_public_key" "example" {
  name                = "user2"
  resource_group_name = "DefaultResourceGroup-EUS"
  count               = var.create_new_key_pair ? 0 : 1
}

resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
  count     = var.create_new_key_pair ? 1 : 0

}

resource "local_file" "ssh_key" {
  filename             = "user1.pem"
  file_permission      = "400"
  directory_permission = "700"
  content              = length(tls_private_key.ssh) > 0 ? tls_private_key.ssh[0].private_key_pem : ""
}

# Resource group
resource "azurerm_resource_group" "example" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_network_security_group" "example" {
  name                = "example-nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# Virtual network
resource "azurerm_virtual_network" "example" {
  name                = var.virtual_network_name
  address_space       = var.virtual_network_address_space
  location            = var.location
  resource_group_name = azurerm_resource_group.example.name
}

# Subnet
resource "azurerm_subnet" "example" {
  name                 = var.subnet_name
  address_prefixes     = var.subnet_address_prefixes
  virtual_network_name = azurerm_virtual_network.example.name
  resource_group_name  = azurerm_resource_group.example.name
}

# Public IP
resource "azurerm_public_ip" "example" {
  name                = var.public_ip_name
  location            = var.location
  resource_group_name = azurerm_resource_group.example.name
  allocation_method   = "Dynamic"
}

# Network interface
resource "azurerm_network_interface" "example" {
  name                = var.network_interface_name
  location            = var.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "my-nic-ip-config"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.example.id
  }
}
resource "azurerm_network_interface_security_group_association" "example" {
  network_interface_id      = azurerm_network_interface.example.id
  network_security_group_id = azurerm_network_security_group.example.id
}
# Virtual machine
resource "azurerm_linux_virtual_machine" "example" {
  name                            = var.vm_name
  location                        = var.location
  resource_group_name             = azurerm_resource_group.example.name
  network_interface_ids           = [azurerm_network_interface.example.id]
  admin_username                  = var.vm_admin_username
  size                            = var.size
  disable_password_authentication = true
  custom_data                     = filebase64("software.sh")

  os_disk {
    caching              = var.os_disk_caching
    storage_account_type = var.os_disk_managed_disk_type
  }
  source_image_reference {
    publisher = var.image_publisher
    offer     = var.image_offer
    sku       = var.image_sku
    version   = var.image_version
  }

  admin_ssh_key {
    username   = var.vm_admin_username
    public_key = length(tls_private_key.ssh) > 0 ? tls_private_key.ssh[0].public_key_openssh : data.azurerm_ssh_public_key.example[0].public_key
  }
}
