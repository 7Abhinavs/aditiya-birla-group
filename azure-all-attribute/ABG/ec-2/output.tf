#output "public-ip" {
 # value = azurerm_public_ip.example.ip_address
#}

#output "vm_id" {
 # value = azurerm_linux_virtual_machine.example.id
#}
output "public-ip" {
  value = data.azurerm_public_ip.example.ip_address
  
}

output "vm_id" {
  value = azurerm_linux_virtual_machine.example.id
}


data "azurerm_public_ip" "example" {
  name = var.public_ip_name
  resource_group_name = var.resource_group_name
  depends_on = [
    azurerm_linux_virtual_machine.example
  ]
}
