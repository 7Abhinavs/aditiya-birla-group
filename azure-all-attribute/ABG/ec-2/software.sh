#!/bin/bash
sudo apt-get update -y

# ####rsyslog for linux####

echo "*.info    @10.17.62.4:514" >> /etc/rsyslog.conf
sudo systemctl restart rsyslog
# sudo systemctl status rsyslog

####Falcon Installation#####

telnet ts01-gyr-maverick.cloudsink.net 443
sleep 5
telnet lfodown01-gyr-maverick.cloudsink.net 443
sleep 5
sudo apt-get install libnl-3-200
sudo wget https://abg-software.s3.ap-south-1.amazonaws.com/falcon-sensor_6.40.0-13706_amd64.deb
sudo dpkg -i ./falcon-sensor_6.40.0-13706_amd64.deb
sudo /opt/CrowdStrike/falconctl -s --aph=185.46.212.90 --app=80
sudo /opt/CrowdStrike/falconctl -s --apd=FALSE
sudo /opt/CrowdStrike/falconctl -s --cid=21C21837B7534213BD1E49FFFC9121FA-A4
sudo service falcon-sensor start 
sudo ps -e | grep falcon-sensor

####NTP Installation On####

sudo timedatectl set-timezone Asia/Kolkata
date 
sudo apt install ntp -y
sudo apt install ntpdate -y
sudo cp /etc/ntp.conf /etc/ntp.conf-bak
sudo su
echo "server 10.17.69.8" >> /etc/ntp.conf
sed -i '/^pool/s/^/#/' /etc/ntp.conf
sudo systemctl restart ntp.service
ntpq -pn
ntpdate -u 10.17.69.8




sudo -i
# quietly add a user without password
adduser --quiet --disabled-password --shell /bin/bash --home /home/newuser --gecos "User" sushant
# set password
echo "sushant:Abhinav6" | sudo chpasswd
#admin access
sudo usermod -aG sudo sushant
#To login using password
cd /etc/ssh
#To make password login
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' sshd_config
#To restart ssh
sudo service ssh restart
