# Variables
variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "virtual_network_name" {
  type = string
}

variable "virtual_network_address_space" {
  type    = list(string)
  default = ["10.0.0.0/16"]
}

variable "subnet_name" {
  type = string
}

variable "subnet_address_prefixes" {
  type    = list(string)
  default = ["10.0.1.0/24"]
}

variable "public_ip_name" {
  type = string
}

variable "network_interface_name" {
  type = string
}

variable "vm_name" {
  type = string
}

variable "vm_admin_username" {
  type = string
}

variable "vm_admin_password" {
  type = string
}

variable "image_publisher" {
  type    = string
  default = "Canonical"
}

variable "image_offer" {
  type    = string
  default = "UbuntuServer"
}

variable "image_sku" {
  type    = string
  default = "16.04-LTS"
}

variable "image_version" {
  type    = string
  default = "latest"
}

variable "os_disk_name" {
  type    = string
  default = "my-vm-os-disk"
}

variable "os_disk_caching" {
  type    = string
  default = "ReadWrite"
}

variable "os_disk_create_option" {
  type    = string
  default = "FromImage"
}

variable "os_disk_managed_disk_type" {
  type    = string
  default = "Standard_LRS"
}

variable "size" {

}

variable "key_name" {

}

variable "create_new_key_pair" {
  type    = bool
}
