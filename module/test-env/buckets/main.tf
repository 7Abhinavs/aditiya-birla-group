module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = var.bucket-name
  acl    = var.acl

  versioning = {
    enabled = true
  }

}