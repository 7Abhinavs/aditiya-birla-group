resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.key_name
  public_key = tls_private_key.example.public_key_openssh
}

resource "local_file" "ssh_key" {
  filename             = "${aws_key_pair.generated_key.key_name}.pem"
  file_permission      = "400"
  directory_permission = "700"
  content              = tls_private_key.example.private_key_pem
}

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = var.ec2-name

  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  associate_public_ip_address = true
  monitoring                  = false
  vpc_security_group_ids      = [var.vpc_security_group_ids]
  subnet_id                   = var.subnet_id
  user_data                   = "${file("./software.sh")}"
  depends_on = [
    aws_key_pair.generated_key
  ]
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

output "ec2_instance" {
  value = module.ec2_instance.public_ip
}