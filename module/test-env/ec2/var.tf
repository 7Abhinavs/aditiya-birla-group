variable "ec2-name" {
  default = "test-ec2"
}

variable "ami" {
  default = "ami-0f8ca728008ff5af4"
}

variable "instance_type" {
  default = "t2.nano"
}

variable "key_name" {
  default = "user1"
}

variable "vpc_security_group_ids" {
  default = "sg-0a6a237fd75a777a9"
}

variable "subnet_id" {
  default = "subnet-0caff162e254ea794"
}
