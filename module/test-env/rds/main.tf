module "db" {
  source = "terraform-aws-modules/rds/aws"

  identifier = var.identifier

  engine            = var.engine
  engine_version    = var.engine_version
  instance_class    = var.instance_class
  allocated_storage = var.allocated_storaged

  db_name  = var.db_name
  username = var.username
  port     = var.port

  iam_database_authentication_enabled = var.iam_database_authentication_enabled

  vpc_security_group_ids = [var.vpc_security_group_ids]

  # Enhanced Monitoring - see example for details on how to create the role
  # by yourself, in case you don't want to create it automatically
  monitoring_interval    = var.monitoring_interval
  monitoring_role_name   = var.create_monitoring_role
  create_monitoring_role = var.create_monitoring_role
  storage_encrypted      = var.storage_encrypted
  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  # DB subnet group
#   create_db_subnet_group = true
#   subnet_ids             = [local.private-subnet-id[0], local.private-subnet-id[1]]

  # DB parameter group
  family = var.family

  # DB option group
  major_engine_version = var.major_engine_version

  # Database Deletion Protection
  deletion_protection = var.deletion_protection
}
