variable "identifier" {
  default = "test-db"
}

variable "engine" {
  default = "mysql"
}

variable "engine_version" {
  default = "8.0.30"
}

variable "instance_class" {
  default = "db.t2.micro"
}

variable "allocated_storaged" {
  default = "5"
}

variable "db_name" {
  default = "demo-db"
}

variable "username" {
  default = "user"
}

variable "port" {
  default = "3306"
}

variable "iam_database_authentication_enabled" {
  default = "false"
}

variable "vpc_security_group_ids" {
    default = "vpc-05c7fc7c5b084bbeb"
}

variable "monitoring_interval" {
  default = "30"
}

variable "monitoring_role_name" {
  default = "MyRDSMonitoringRole"
}

variable "create_monitoring_role" {
  default = "true"
}

variable "storage_encrypted" {
  default = "false"
}

variable "family" {
  default = "mysql8.0"
}

variable "major_engine_version" {
  default = "8"
}

variable "deletion_protection" {
  default = "true"
}